Name:          libsodium
Summary:       The Sodium crypto library
Version:       1.0.20
Release:       1
License:       ISC
URL:           https://github.com/jedisct1/libsodium
Source0:       https://github.com/jedisct1/libsodium/releases/download/%{version}-RELEASE/%{name}-%{version}.tar.gz

BuildRequires: gcc
BuildRequires: make
Obsoletes:     libsodium23 <= %{version}

%description
Sodium is a modern, easy-to-use software library for encryption, decryption, signatures, password
hashing and more. It is a portable, cross-compilable, installable6, packageable fork of NaCl, with
a compatible API, and an extended API to improve usability even further.

Its goal is to provide all of the core operations needed to build higher-level cryptographic tools.

%package       devel
Summary:       Development headers and libraries files for libsodium

Requires:      libsodium = %{version}-%{release}
Provides:      libsodium-static = %{version}-%{release}
Obsoletes:     libsodium23-devel <= %{version} libsodium-static < %{version}-%{release}

%description   devel
This package contains libraries and header files for developing applications that use libsodium libraries.

%prep
%autosetup -p1

%build
%configure --disable-silent-rules --disable-opt
%make_build

%install
%make_install
%delete_la

%check
%make_build check

%files
%license LICENSE
%{_libdir}/libsodium.so.26*

%files devel
%doc AUTHORS ChangeLog README.markdown THANKS
%doc test/default/*.{c,exp,h} test/quirks/quirks.h
%{_includedir}/sodium.h
%{_includedir}/sodium/
%{_libdir}/libsodium.so
%{_libdir}/pkgconfig/libsodium.pc
%{_libdir}/libsodium.a

%changelog
* Mon Jul 29 2024 Funda Wang <fundawang@yeah.net> - 1.0.20-1
- update to 1.0.20

* Tue Oct 17 2023 yaoxin <yao_xin001@hoperun.com> - 1.0.19-1
- Upgrade to 1.0.19

* Mon Jul 27 2020 lingsheng <lingsheng@huawei.com> - 1.0.18-1
- update to 1.0.18

* Thu Jan 15 2019 Lei Zhang <ricky.z@huawei.com> - 1.0.16-7
- modify the unavailable source address

* Wed Nov 13 2019 Lijin Yang <yanglijin@huawei.com> - 1.0.16-6
- init package
